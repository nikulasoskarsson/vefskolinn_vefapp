const admin = require("firebase-admin");

const serviceAccount = require("./Firebase-admin-SDK.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://vefskolinn-app.firebaseio.com"
});

// This registration token comes from the client FCM SDKs.
const registrationToken = "dHI4rbeAE7g:APA91bFQ690niSU-8X6BVSdLRP7ADRcRN0q8NwhUvIdhOTmxiNBdLZWOD37uWYkakcyQ-zn3FqpbkC9dsuRJRQsEMaEaMCUOpSY7k0MUtx1n9juaZhAJbMjb2vmK3zZDzm4NPCqlB4Ge"

const message = {
  data: {
    title: 'Vefskólinn',
    message: 'Minni á próf á þriðjudag',
    icon: 'img/Tskoli-logo.png'

  },

  token: registrationToken
};

// Send a message to the device corresponding to the provided
// registration token.
admin.messaging().send(message)
  .then((response) => {
    // Response is a message ID string.
    console.log('Successfully sent message:', response);
  })
  .catch((error) => {
    console.log('Error sending message:', error);
  });