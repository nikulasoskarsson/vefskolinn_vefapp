require('./globalFunctions')

test('If getCurrentDate is defined', () => {
    expect(getCurrentDate(new Date())).toBeDefined()
})

test('If getCurrentDate has been called', () => {
    expect(getCurrentDate(new Date())).not.toBe( Object )
})