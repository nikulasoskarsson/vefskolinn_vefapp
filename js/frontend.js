const logOutBtn = document.getElementById('btn-logout');
const createAdminBtn = document.getElementById('btn-create-admin');
logUserOut = () => {
  auth.signOut().then(() => {});
};
// add admin cloud functions
createAdmin = () => {
  const adminEmail = document.getElementById('create-admin-email').value;
  const addAdminRole = functions.httpsCallable('addAdminRole');

  addAdminRole({ email: adminEmail })
    .then(result => {
      alert(result.data.message);
    })
    .catch(err => {
      console.log('error' + err);
    });
};
createAdminBtn.addEventListener('click', createAdmin);
logOutBtn.addEventListener('click', logUserOut);
