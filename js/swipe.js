

//// Slip.js library fyrir sipe+delete virkni í biðlista(mobile) 

const swipeContainer = document.getElementById("swipe-container");
// const swipeDiv = document.querySelectorAll(".swipe");
const swipeDiv = document.querySelectorAll(".card-container");
const home = document.querySelector("#modal-container");


new Slip(swipeContainer);

for (let i = 0; i < swipeDiv.length; i++) {
    swipeDiv[i].addEventListener("slip:swipe", () => {
        console.log("virkar");
        home.innerHTML = `
       <div class="eydaModal" id="add-content-modal">
           <div class="message-modal modal-content">
               <div class="close-modal">
                   <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                       <path fill-rule="evenodd" clip-rule="evenodd" d="M18.7071 5.29289C19.0976 5.68342 19.0976 6.31658 18.7071 6.70711L6.70711 18.7071C6.31658 19.0976 5.68342 19.0976 5.29289 18.7071C4.90237 18.3166 4.90237 17.6834 5.29289 17.2929L17.2929 5.29289C17.6834 4.90237 18.3166 4.90237 18.7071 5.29289Z" fill="#8F9299"/>
                       <path fill-rule="evenodd" clip-rule="evenodd" d="M5.29289 5.29289C5.68342 4.90237 6.31658 4.90237 6.70711 5.29289L18.7071 17.2929C19.0976 17.6834 19.0976 18.3166 18.7071 18.7071C18.3166 19.0976 17.6834 19.0976 17.2929 18.7071L5.29289 6.70711C4.90237 6.31658 4.90237 5.68342 5.29289 5.29289Z" fill="#8F9299"/>
                   </svg> 
               </div>
               <h3 class="modal-heading">Eyða af biðlista&#63;</h3>
               <p>Ertu viss um að þú viljir eyða þér af biðlistanum&#63;</p>
               <div class="modal-btn-container">
                   <button class="btn-danger form-btn">
                       Nei
                   </button>
                   <button class="btn-success form-btn">
                       Já
                   </button>
               </div>
           </div>
       </div>
       `
        const yesBtn = document.querySelector(".btn-success");
        const noBtn = document.querySelector(".btn-danger");
        const closeBtn = document.querySelector(".close-modal")

        yesBtn.addEventListener("click", () => {
            swipeContainer.removeChild(swipeDiv[i]);
            home.innerHTML = ``
        });

        const closeModal = () => {
            home.innerHTML = ``
        }
        noBtn.addEventListener("click", closeModal)
        closeBtn.addEventListener("click", closeModal)

    });
}













