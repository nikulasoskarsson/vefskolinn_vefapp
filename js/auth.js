// Initialize Firebase
var config = {
  apiKey: 'AIzaSyA62e6x5KSg4JK0XyV5Ww9xAKSq9ae4Bcc',
  authDomain: 'vefskolinn-app.firebaseapp.com',
  databaseURL: 'https://vefskolinn-app.firebaseio.com',
  projectId: 'vefskolinn-app',
  storageBucket: 'vefskolinn-app.appspot.com',
  messagingSenderId: '378878795682'
};
firebase.initializeApp(config);
const auth = firebase.auth();
const db = firebase.firestore();
const functions = firebase.functions();

auth.onAuthStateChanged(user => {
  // Ef einhver er loggaður inn
  if (user) {
    // Þarf að ná í user info úr gagnagrunninum því ananrs er redirectað of snemma í sign up ferlinu og það næst ekki að fara í gegn.
    //Þetta keyrir bara eftir að það er actually til stak í users gagnagrunninum sem matcahar uid-inu
    db.collection('users')
      .doc('7ZWwDFynPOeFLSZzmRhplqDt3xI2')
      .get()
      .then(user => {
        if (
          // Redirect á viðeigandi síður
          window.location.pathname == '/index.html' ||
          window.location.pathname == '/signUp.html'
        ) {
          window.location.href = 'frontend.html';
        }
      });

    user.getIdTokenResult().then(idTokenResult => {
      if (idTokenResult.claims.admin) {
        const adminUi = document.querySelectorAll('.admin');
        adminUi.forEach(element => {
          element.style.visibility = 'visible'; // Sýna admin ui-ið
        });
      }
    });
  } else {
    // Ef Notandi fer á frontend.html en er ekki skráður inn fær hann redirect á login síðuna
    if (window.location.pathname == '/frontend.html') {
      window.location.href = 'index.html';
    }
  }
});
