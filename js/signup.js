// email function I found online change later
function ValidateEmail(mail) {
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
    return true;
  }
  alert('You have entered an invalid email address!');
  return false;
}
//Mismunandi states eftir hvaðas stigi maður er á í sign up
const formContentHolder = [
  {
    id: 0,
    heading: 'Hver er kennitalan þín &#63',
    placeholder: 'Kennitala',
    value: 'kennitala'
  },
  {
    id: 1,
    heading: 'Hvað er emailið þitt &#63',
    placeholder: 'Email',
    value: 'email'
  },
  {
    id: 2,
    heading: 'Veldu þér lykilorð',
    placeholder: 'Lykilorð',
    value: 'lykilord'
  },
  {
    id: 3,
    heading: 'Staðfestu lykilorð',
    placeholder: 'Staðfestu lykilorð',
    value: 'lykilord-comfirm'
  }
];
const nemendur = [
  {
    forNafn: 'Nikulás',
    midjuNafn: '',
    eftirNafn: 'Óskarsson',
    kennitala: '1911952219',
    hopur: 4,
    imgSrc: './img/nikulas.jpg',
    kennari: false
  },
  {
    forNafn: 'Anton',
    midjuNafn: 'Örn',
    eftirNafn: 'Kærnested',
    kennitala: '1207952879',
    hopur: 4,
    imgSrc: './img/anton.png',
    kennari: false
  },
  {
    forNafn: 'Hákon',
    midjuNafn: 'Arnar',
    eftirNafn: 'Sigurðsson',
    kennitala: '1302963279',
    hopur: 4,
    imgSrc: './img/hakon.jpg',
    kennari: false
  },
  {
    forNafn: 'Guðmundur',
    midjuNafn: 'Helgi',
    eftirNafn: 'Rósuson',
    kennitala: '0607842999',
    hopur: 4,
    kennari: false
  },
  {
    forNafn: 'Björgvin',
    midjuNafn: 'Atli',
    eftirNafn: 'Snorrason',
    kennitala: '2708902539',
    hopur: 4,
    kennari: false
  }
];
//Virkni fyrir sign up
const signUpForm = document.getElementById('signup');
const signUpInfo = [];
const backBtnContainer = document.getElementById('progress-title');

let i = 0; // global teljari notaður til að halda utan um state á sign up forminu

// signUpUser = () => {
//   inputBtn = document.querySelector(".input-btn");
// };
changeHtml = counter => {
  i++; // Teljari hækkaður um einn til þess að matcha við næsta gæja í arrayinu
  signUpForm.innerHTML = returnDiv(i);
};

//Athuga hvort kennitala sé þegar til í gagnagrunni s.s nemandi er þegar búinn að skrá sig
checkIfKennitalaExists = kennitala => {
  let bool = false;
  return db
    .collection('users')
    .get()
    .then(snapshot => {
      snapshot.forEach(data => {
        const currentKennitala = data.data().kennitala;
        if (currentKennitala === kennitala) {
          bool = true;
        }
      });
      return bool;
    });
};
checkIfKennitalaExists('1911952219').then(data => {
  console.log(data);
});
// function sem tekur inn eitt object í einu og returnar divi með upplýsinguuum úr því
returnDiv = counter => {
  return `   
    
<div class="progress-title">
  <i class="fas fa-arrow-left back-btn"></i>
  <h3 class="form-heading">
    ${formContentHolder[counter].heading}
  </h3>
</div>
<div class="Input-Container">
  <input
    class="input-field signup-input"
    placeholder="${formContentHolder[counter].placeholder}"
  />
  <div class="input-btn">
    <i class="input-arrow fas fa-arrow-right forward-btn"></i>
  </div>
</div>
<i class="fas fa-arrow-left back-btn-m"></i>

`;
};
//Vildumeldingar skilaboð
showErrMessage = message => {
  const msgDiv = document.querySelector('.signup-message');
  msgDiv.innerHTML = message;
  msgDiv.style.color = 'red';
};
//Success skilaboð
showSuccsessMessage = message => {
  const msgDiv = document.querySelector('.signup-message');
  msgDiv.innerHTML = message;
  msgDiv.style.color = 'green';
};
const returnObject = {};
getData = counter => {
  const inputValue = document.querySelector('.signup-input').value;

  if (counter === 0) {
    let boolean = false;
    nemendur.forEach(nemandi => {
      if (nemandi.kennitala === inputValue) {
        boolean = true;
        checkIfKennitalaExists(inputValue).then(bool => {
          if (bool) {
            boolean = true;
            showErrMessage(
              'Þú ert þegar skráður hjá okkur, hafðu samband við kennara ef þú hefur gleymt lykilorði eða netfangi'
            );
          } else {
            boolean = true;
            returnObject.kennitala = inputValue;
            showSuccsessMessage(`Hæ, ${nemandi.forNafn}`);

            changeHtml(counter);
            document.querySelector('.progress-bar-2').style.backgroundColor =
              '#6fcf97';
          }
        });
      }
    });
    if (!boolean) {
      showErrMessage(
        'Villa, Kennitala fannst ekki í gagnagrunni yfir nemendur'
      );
    }
  } else if (counter === 1) {
    if (ValidateEmail(inputValue)) {
      returnObject.email = inputValue;
      document.querySelector('.progress-bar-3').style.backgroundColor =
        '#6fcf97';
      changeHtml(counter);
    }
  } else if (counter === 2) {
    if (inputValue.length < 4) {
      showErrMessage('Lykilorð verður að vera allavegana 3 char');
    } else {
      returnObject.lykilord = inputValue;
      showSuccsessMessage('Looking good');
      changeHtml(counter);
      document.querySelector('.progress-bar-4').style.backgroundColor =
        '#6fcf97';
    }
  } else if (counter === 3) {
    if (returnObject.lykilord != inputValue) {
      showErrMessage('Lykilorð og er ekki sama');
    } else {
      document.querySelector('.progress-bar-5').style.backgroundColor =
        '#6fcf97';
      returnObject.comfirmedLykilord = inputValue;
      auth
        .createUserWithEmailAndPassword(
          returnObject.email,
          returnObject.lykilord
        )
        .then(cred => {
          // þegar createUserWithEmailAndPassword functonið sem er async er búið að keyra
          let firstName, middleName, lastName, fullName, group, img, isKennari;
          nemendur.forEach(nemandi => {
            if (nemandi.kennitala == returnObject.kennitala) {
              firstName = nemandi.forNafn;
              middleName = nemandi.midjuNafn;
              lastName = nemandi.eftirNafn;
              group = nemandi.hopur;
              img = nemandi.imgSrc;
              isKennari = nemandi.kennari;
              if (middleName.length === 0) {
                // Ef það er ekkert middle name
                fullName = firstName + ' ' + lastName;
              } else {
                fullName = firstName + ' ' + middleName + ' ' + lastName;
              }
            }
          });

          db.collection('users') // Búið til nýtt stak í gagnagrunni sem heldur utan um meira info um notenda
            .doc(cred.user.uid)
            .set({
              name: fullName,
              firstName: firstName,
              middleName: middleName,
              lastName: lastName,
              kennitala: returnObject.kennitala,
              email: returnObject.email,
              password: returnObject.lykilord,
              hopur: group,
              myndUrl: img,
              isKennari: isKennari,
              userId: cred.user.uid
            })
            .then(() => {});
        });
    }
  }
  return returnObject;
};

signUpForm.addEventListener('click', event => {
  if (event.target.className === 'input-btn') {
    if (i <= formContentHolder.length - 1) {
      console.log('click');
      signUpInfo.push(getData(i));

      // signUpUser();
    }
  }
});
//Function sem að breytir lit á active state divunum
showActiveState = i => {
  const activeStates = document.querySelectorAll('.active-state');
  activeStates.forEach((state, index) => {
    if (i <= index - 1) {
      state.style.backgroundColor = '#8f9299';
    } else {
      state.style.backgroundColor = '6fcf97';
    }
  });
};

//Function til að fara til baka um eina síðu
goBack = () => {
  if (i === 0) {
    window.location.href = '/index.html';
  } else {
    i = i - 1;
    signUpForm.innerHTML = returnDiv(i);
    showSuccsessMessage('');
    showActiveState(i);
  }
};
signUpForm.addEventListener('click', event => {
  if (event.target.className === 'back-btn') {
    goBack();
  }
});

init = () => {
  signUpForm.innerHTML = returnDiv(i);
};
window.addEventListener('load', init);
window.setInterval(function() {}, 5000);
