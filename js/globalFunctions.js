//Functions sem maður gæti þurft að kalla í úr meira en einni javascript skrá

// Format date function used in many different javascript files
getCurrentDate = date => {
  const months = [
    'Janúar',
    'Febrúar',
    'Mars',
    'Apríl',
    'Maí',
    'Júní',
    'Júlí',
    'Ágúst',
    'September',
    'Október',
    'Nóvember',
    'Desember'
  ];
  return `${date.getDate()} ${months[date.getMonth()]}`;
};

// Function til að displaya cards, async því það þarf að ná í mynd úr öðru data collection
async function displayCards(cardHolder, holder) {
  holder.innerHTML = '';

  let efniCard = '';
  for (let i = 0; i < cardHolder.length; i++) {
    if (cardHolder[i].kennari) {
      efniCard = cardHolder[i];
    } else {
      efniCard = cardHolder[i].data();
    }
    if (efniCard.efni) {
      const div = await returnNotifDiv(efniCard);
      holder.innerHTML += div;
    } else {
      const div = await returnDiv(efniCard);
      holder.innerHTML += div;
    }
  }
}

// Ná í mynd af kennara með að matcha uid á postinum við uid á kennara
getTeacherImage = teacherId => {
  return db
    .collection('users')
    .doc(teacherId)
    .get()
    .then(doc => {
      return doc.data().myndUrl;
    });
};

// module.exports = getCurrentDate;
