//Kóði sem ætti að sjá um virkni fyrir alla modalana. Passsa bara að þeir hafi þessa klassa :)

// const openModal = document.querySelectorAll('.open-modal');
const clodeModal = document.querySelectorAll('.close-modal');

// openModal.forEach(modal => {
//   modal.addEventListener('click', () => {
//     const modal = document.querySelector('.modal');
//     modal.style.display = 'block';
//   });
// });

clodeModal.forEach(button => {
  button.addEventListener('click', () => {
    const modals = document.querySelectorAll('.modal');
    modals.forEach(modal => {
      modal.style.display = 'none';
    });
  });
});

displayModelContent = (modalId, modalHolder) => {
  const modal = document.getElementById(modalHolder);
  console.log(modal);
  modal.style.display = 'block';

  const modalContent = document.getElementById(modalId);
  modalContent.style.display = 'flex';
};

