const form = document.getElementById('add-content-form');
const efniContentHolder = document.getElementById('efni-content-holder');
getNameFromDb = () => {
  return db
    .collection('users')
    .doc(auth.currentUser.uid) // user id á þeim notenda sem er loggaður inn
    .get()
    .then(doc => {
      return doc.data().firstName;
    });
};

form.addEventListener('submit', e => {
  e.preventDefault(); // kemur í veg fyrir reload þegar formið er submittað

  const hopur = form['content-group'].value;
  const afangi = form['content-course'].value;
  const title = form['content-heading'].value;
  const text = form['content-text'].value;
  const linkArray = [];
  const allLinks = document.querySelectorAll('.link');
  const date = new Date();
  allLinks.forEach((link, index) => {
    if (link.value.length > 6) {
      linkArray.push(link.value);
    }
  });
  // Nær í nafið á notenda sem er núna loggaður inn með getNameFrom Db functoninu sem er async og getur síðan sett það í gagnagrunninn
  getNameFromDb()
    .then(name => {
      return name;
    })
    .then(name => {
      db.collection('efni')
        .add({
          kennari: name,
          hópur: hopur,
          áfangi: afangi,
          titill: title,
          hlekkir: linkArray,
          texti: text,
          dagsettning: getCurrentDate(date),
          uid: auth.currentUser.uid
        })
        .then(() => {
          console.log('success');
        })
        .catch(err => {
          console.log(err);
        });
    });
});

db.collection('efni').onSnapshot(snapshot => {
  displayCards(snapshot.docs, efniContentHolder);
});

getClassImage = course => {
  if (course === 'Javascript' || course === 'javascript-2') {
    return 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ-Te6PLFAetMsUZIXskrkCfIlaSo1wCNqpKyKlHaaxgH_NCIUk';
  } else if (course === 'Vefhönnun' || course === 'vefhonnun-2') {
    return 'https://cdn-images-1.medium.com/max/1200/1*DG5eBssbHsAyh_RtTRz8mQ@2x.png';
  }
};
getTeacherImage = teacherId => {
  return db
    .collection('users')
    .doc(teacherId)
    .get()
    .then(doc => {
      return doc.data().myndUrl;
    });
};
returnDiv = card => {
  return getTeacherImage(card.uid).then(url => {
    return `


    <div class="card notification-dtop-container dCard">
        <div class="heads-left">
            <span>${card.dagsettning}</span>
            <div class="course">
                <img src="${getClassImage(card.áfangi)}" class="efni-card-img">
                <p>${card.áfangi}</p>
            </div>
            <p>${card.titill}</p>
        </div>

        <div class="heads-right">
            <div class="kennari">
                <img src="${url}">
                <p>${card.kennari}</p>
            </div>
            <span>${card.hlekkir.length} linkar</span>
              <svg class="margin-lr-small" width="40" height="40" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M3 2C2.73478 2 2.48043 2.10536 2.29289 2.29289C2.10536 2.48043 2 2.73478 2 3V17C2 17.2652 2.10536 17.5196 2.29289 17.7071C2.48043 17.8946 2.73478 18 3 18H19C19.2652 18 19.5196 17.8946 19.7071 17.7071C19.8946 17.5196 20 17.2652 20 17V6C20 5.73478 19.8946 5.48043 19.7071 5.29289C19.5196 5.10536 19.2652 5 19 5H10C9.66565 5 9.35342 4.8329 9.16795 4.5547L7.46482 2H3ZM0.87868 0.87868C1.44129 0.31607 2.20435 0 3 0H8C8.33435 0 8.64658 0.167101 8.83205 0.4453L10.5352 3H19C19.7957 3 20.5587 3.31607 21.1213 3.87868C21.6839 4.44129 22 5.20435 22 6V17C22 17.7957 21.6839 18.5587 21.1213 19.1213C20.5587 19.6839 19.7957 20 19 20H3C2.20435 20 1.44129 19.6839 0.87868 19.1213C0.31607 18.5587 0 17.7957 0 17V3C0 2.20435 0.31607 1.44129 0.87868 0.87868Z" fill="#344762"/>
              </svg>
              <button class="btn-primary">Hlaða niður</button>
              <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z" fill="#344762"/>
              </svg>       
        </div>	
    </div>
  `;
  });
};

getCurrentDate = date => {
  const months = [
    'Janúar',
    'Febrúar',
    'Mars',
    'Apríl',
    'Maí',
    'Júní',
    'Júlí',
    'Ágúst',
    'September',
    'Október',
    'Nóvember',
    'Desember'
  ];
  return `${date.getDate()} ${months[date.getMonth()]}`;
};
