const notificationForm = document.getElementById('notification-form');
const notificationContentHolder = document.getElementById(
  'notif-content-holder'
);

getFullNameFromDb = () => {
  return db
    .collection('users')
    .doc(auth.currentUser.uid) // user id á notenda sem er loggaður inn
    .get()
    .then(doc => {
      return doc.data().name;
    });
};
notificationForm.addEventListener('submit', e => {
  e.preventDefault();
  const group = notificationForm['hopur'].value;
  const title = notificationForm['titill'].value;
  const linkur = notificationForm['linkur'].value;
  const content = notificationForm['content'].value;
  const date = new Date();
  getFullNameFromDb().then(name => {
    db.collection('tilkynningar').add({
      hopur: group,
      titill: title,
      hlekkur: linkur,
      efni: content,
      dagsetning: getCurrentDate(date), // notificationFormata date
      sendandi: name,
      uid: auth.currentUser.uid // Senda með uid þannig hægt er að nota það til að ná í upplýsingar um notednda sem setti in þetta notification seinna
    });
  });
});
getCurrentDate = date => {
  const months = [
    'Janúar',
    'Febrúar',
    'Mars',
    'Apríl',
    'Maí',
    'Júní',
    'Júlí',
    'Ágúst',
    'September',
    'Október',
    'Nóvember',
    'Desember'
  ];
  return `${date.getDate()} ${months[date.getMonth()]}`;
};

// Display content
db.collection('tilkynningar').onSnapshot(snapshot => {
  displayCards(snapshot.docs, notificationContentHolder);
});
//Þarf að stýla seinna og búa til actual card div
returnNotifDiv = card => {
  return ` 
    <div class="card notification-dtop-container">
        <div class="left-stuff">
            <span>${card.dagsetning}</span>
            <p>${card.titill}</p> 
        </div>
        <div class="right-stuff">
            <p>${card.sendandi}</p>
            <button class="btn-primary">Skoða</button>
        </div>
        <div class="alert-icon"></div>	
    </div>
    `;
};
