# Vefskóla app - Innri vefur

"Progressive web app" fyrir vefskólann þar sem nemendur geta m.a skráð sig á biðlista hjá kennurum, skráð sig í viðtal/próf og séð kennsluefni dagsins.   

## Getting Started

Farðu á: "https://gitlab.com/nikulasoskarsson/vefskolinn_vefapp" og forkaðu repository-ið .. Fork it! 
Búðu til nýtt verkefni/möppu í vefaranum þínum. Farðu í terminal-ið og git clone-aðu þig í gang(git clone "fork-slóðin").  

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites - ### Installing

Bættu við "yarn" package manager í verkefnið með því að skrifa í terminal-ið: yarn , og svo í framhaldi: yarn init .. 

What things you need to install the software and how to install them

```
Give examples
```

### Installing

Farðu á: "https://gitlab.com/nikulasoskarsson/vefskolinn_vefapp" og forkaðu repository-ið .. Fork it! 
Búðu til nýtt verkefni/möppu í vefaranum þínum. Farðu í terminal-ið og git clone-aðu þig í gang(git clone "fork-slóðin").

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

## Built With

* [Firebase](https://firebase.google.com/docs) - Fyrir almenna virkni fyrir afturendann á síðunni. Nánar tiltekið til að búa til reikning fyrir notendur og skrá      þá inn. Og einnig til að halda utan um gögn sem sem eru birt á síðunni. 
* [Slip.js](https://github.com/kornelski/slip) - Javascript library sem gerir okkur kleyft að nota swipe virkni til að eyða út kortum í farsíma útgáfunni.

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Anton Örn Kærnestad** 
* **Björgvin Atli Snorrason** 
* **Guðmundur Helgi Rósuson** 
* **Hákon Arnar Sigurðsson** 
* **Nikulás Óskarsson** 

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License


## Acknowledgments

* Mr.Kornelski fær þakkir fyrir að búa til slip.js library-ið og hafa það opið öllum - https://github.com/kornelski/slip
