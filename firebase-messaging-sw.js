// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
    'messagingSenderId': '378878795682'
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    var notificationTitle = payload.data.title;
    var notificationOptions = {
        body: 'texti',
        icon: 'https://avatars3.githubusercontent.com/u/34031668?s=280&v=4',
        click_action: 'https://vefskolinn.is',
        actions: [
            {action: 'like', title: 'Like', icon: 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/sign-check-icon.png'}
        ]
        // body: payload.data.icon,
        // icon: payload.data.icon,
        // badge: payload.data.icon,
        // image: payload.data.icon,
        // vibrate: [200,100,200]
    };

    return self.registration.showNotification(notificationTitle,
        notificationOptions);
});
